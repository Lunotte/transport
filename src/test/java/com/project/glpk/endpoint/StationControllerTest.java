/*package com.project.glpk.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.project.entity.User;
import com.project.glpk.model.Station;
import com.project.glpk.service.DetailService;
import com.project.glpk.service.StationService;
import com.project.user.service.DatabaseUserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = DetailController.class, secure = false)
public class StationControllerTest {

	@Autowired
    private MockMvc mockMvc;
 
	@MockBean 
	private StationService stationService;
    @MockBean 
	private DetailService detailService;
    @MockBean 
	private DatabaseUserService databaseUserService;
 
    //Add WebApplicationContext field here.
 
    //The setUp() method is omitted.
 
    @Test
	public void retrieveDetailsForCourse() throws Exception {
		User mockUser = new User(1L, "test1234", "$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G");
		List<Station> stations = new ArrayList<Station>(); 
		Station station = new Station(1L, "Angers", 1);
		stations.add(station);
		
		Mockito.when(stationService.getAllStation(mockUser)).thenReturn(stations);
	
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"api/stations").accept(
				MediaType.APPLICATION_JSON);
	
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
	
		System.out.println(result.getResponse());
		String expected = "[{\"id\": 1,\"name\": \"Angers\",\"rank\": 1,\"networks\": [{\"id\": 1,\"name\": \"France\"}],\"biFlux\": false}}";
	
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
    }
}
*/