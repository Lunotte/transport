/*package com.project.glpk.endpoint;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.project.entity.User;
import com.project.security.model.UserContext;
import com.project.user.service.DatabaseUserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = MasterController.class, secure = false)
public class MasterControllerTest {

	@Autowired
    private MockMvc mockMvc;
 
	@MockBean
	private DatabaseUserService databaseUserService;
	
	@Test
	public void getUser() throws Exception {
		//String mockUser = "{\"id\"=1, \"username\"=\"test1234\", \"password\"=\"$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G\"}";
		User mockUser = new User(1L, "test1234", "$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G");
		
		UserContext mockUserContexte = UserContext.create("test1234", null);
		Mockito.when(databaseUserService.getByUsername(mockUserContexte.getUsername())).thenReturn(mockUser);
				
	}
}*/
