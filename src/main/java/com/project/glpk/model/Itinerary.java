package com.project.glpk.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="ITINERARY")
public class Itinerary {
	
	@Id @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToMany
	private List<StationForItinerary> stationForItinerary;

	@Column(name="time")
	private int timeCourse;
	
	public List<StationForItinerary> getStationForItinerary() {
		return stationForItinerary;
	}
	public void setStationForItinerary(List<StationForItinerary> stationForItinerary) {
		this.stationForItinerary = stationForItinerary;
	}
	public int getTimeCourse() {
		return timeCourse;
	}
	public void setTimeCourse(int timeCourse) {
		this.timeCourse = timeCourse;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
