package com.project.glpk.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
@Entity
@Table(name="STATION")
public class Station {
	
	@Id @Column(name="STATION_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME")
	private String name;
	
	@OneToMany(mappedBy="idStationB")
	//@JsonBackReference
	@JsonIgnore
	private List<Detail> details;
	
	@Column(name="RANK")
	private int rank;
	
	@ManyToMany()
    @JoinTable(name = "STATION_NETWORK", joinColumns = @JoinColumn(name = "STATION_ID"),
    inverseJoinColumns = @JoinColumn(name = "NETWORK_ID"))
	//@JsonIgnore
	private List<Network> networks;
	
	@Transient
	private boolean biFlux;
	
	/*@OneToOne
	@JoinColumn(name="STATION_A_ID", referencedColumnName="STATION_ID", insertable=false, updatable=false)
	private Station idStationA;*/
		
	
	
	public String getName() {
		return name;
	}
	
	public Station() {
		super();
	}

	public Station(Long id, String name, int rank) {
		this.id = id;
		this.name = name;
		this.rank = rank;
	}
	
	public List<Network> getNetworks() {
		return networks;
	}

	public void setNetworks(List<Network> networks) {
		this.networks = networks;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Detail> getDetails() {
		return details;
	}
	public void setDetails(List<Detail> details) {
		this.details = details;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public boolean isBiFlux() {
		return biFlux;
	}
	public void setBiFlux(boolean biFlux) {
		this.biFlux = biFlux;
	}
	@Override
	public String toString() {
		return "Station [id=" + id + ", name=" + name + ", detail=" + details + ", rank=" + rank + "]";
	}
	
}
