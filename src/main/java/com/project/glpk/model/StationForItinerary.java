package com.project.glpk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="STATIONFORITINERARY")
public class StationForItinerary {
	
	@Id @Column(name="STATIONFORITI_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="RANK")
	private int rank;
	
	//@Column(name="DETAIL")
	@OneToOne
	private Detail detail;
	
	public StationForItinerary() {
	}

	public StationForItinerary(Long id, String name, int rank, Detail detail) {
		this.id = id;
		this.name = name;
		this.rank = rank;
		this.detail = detail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Detail getDetail() {
		return detail;
	}

	public void setDetail(Detail detail) {
		this.detail = detail;
	}

}
