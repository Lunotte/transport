package com.project.glpk.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.entity.User;

@Entity
@Table(name="NETWORK")
public class Network {

	@Id @Column(name="NETWORK_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@ManyToMany(mappedBy = "networks")
	@JsonIgnore
	private List<Station> stations;
	
	@ManyToOne
	@JoinColumn(name="AUSER", referencedColumnName="ID")
	private User auser;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Station> getStations() {
		return stations;
	}
	public void setStations(List<Station> stations) {
		this.stations = stations;
	}
	@JsonIgnore
	public User getUser() {
		return auser;
	}
	@JsonIgnore
	public void setUser(User user) {
		this.auser = user;
	}
}
