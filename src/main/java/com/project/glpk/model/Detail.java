package com.project.glpk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
@Entity
@Table(name="DETAIL")
public class Detail {
	
	@Id @Column(name="DETAIL_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="STATION_A_ID", referencedColumnName="STATION_ID")
	//@JsonManagedReference
	private Station idStationA;
	
	@ManyToOne
	@JoinColumn(name="STATION_B_ID", referencedColumnName="STATION_ID")
	//@JsonManagedReference
	private Station idStationB;
	
	public Detail() {}
	
	public Detail(Long id, int time) {
		this.id = id;
		this.time = time;
	}
	
	
	public Detail(Long id, Station idStationA, Station idStationB, int time) {
		this.id = id;
		this.idStationA = idStationA;
		this.idStationB = idStationB;
		this.time = time;
	}



	@Column(name="time")
	private int time;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Station getIdStationA() {
		return idStationA;
	}
	public void setIdStationA(Station idStationA) {
		this.idStationA = idStationA;
	}
	public Station getIdStationB() {
		return idStationB;
	}
	public void setIdStationB(Station idStationB) {
		this.idStationB = idStationB;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "Detail [id=" + id + ", idStationA=" + idStationA + ", idStationB=" + idStationB + ", time=" + time
				+ "]";
	}
	
}
