package com.project.glpk.model;

import java.util.List;

public class Objective {

	private Itinerary itinerary;
	private List<Paire> paires;
	
	public Objective(Itinerary itinerary, List<Paire> paires) {
		this.itinerary = itinerary;
		this.paires = paires;
	}
	
	public Itinerary getItinerary() {
		return itinerary;
	}

	public void setItinerary(Itinerary itinerary) {
		this.itinerary = itinerary;
	}

	public List<Paire> getPaires() {
		return paires;
	}
	public void setPaires(List<Paire> paires) {
		this.paires = paires;
	}
	
	
}
