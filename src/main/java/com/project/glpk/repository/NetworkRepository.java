package com.project.glpk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.project.entity.User;
import com.project.glpk.model.Network;

public interface NetworkRepository extends JpaRepository<Network, Long>{
	public List<Network> findByAuser(@Param("name") User user);
	
	public Network findByName(@Param("name") String name);
	
	public Network findById(@Param("id") Long id);

	/*@Query(value="select * from Network limit = ?1", nativeQuery = true)
	public Network findFirstIdOrderByIdDesc(int id);*/
	
}
