package com.project.glpk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.project.entity.User;
import com.project.glpk.model.Detail;
import com.project.glpk.model.Station;

public interface DetailRepository extends JpaRepository<Detail, Long>{
	@Query("select d from Detail d join d.idStationA s join s.networks n where n.auser = ?1")
	public List<Detail> findByUser(User user);
	
	public Detail findByIdStationAAndIdStationB(@Param("idStationA") Station idStationA, @Param("idStationB") Station idStationB);
	
	@Modifying
    @Transactional
    @Query("delete from Detail d where d.idStationA = ?1 or d.idStationB = ?1")
    void deleteDetailByIdStationAOrIdStationB(Station stationId);
}
