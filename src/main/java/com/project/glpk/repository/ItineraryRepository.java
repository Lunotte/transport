package com.project.glpk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.project.glpk.model.Itinerary;

public interface ItineraryRepository extends JpaRepository<Itinerary, Long>{
	public List<Itinerary> findAll();
	
	public Itinerary findById(@Param("id") Long id);

}
