package com.project.glpk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.project.entity.User;
import com.project.glpk.model.Network;
import com.project.glpk.model.Station;

public interface StationRepository extends JpaRepository<Station, Long>{
	@Query("select s from Station s join s.networks n where n.auser = ?1")
	public List<Station> findByAuser(User user);
	
	public List<Station> findByNetworks(@Param("network") Network network);
	
	public Station findTopRankByNetworksOrderByRankDesc(@Param("network") Network network);
	
	public Station findByName(@Param("name") String name);
	
	public Station findById(@Param("id") Long id);


}
