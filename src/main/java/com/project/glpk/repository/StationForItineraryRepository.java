package com.project.glpk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.project.glpk.model.StationForItinerary;

public interface StationForItineraryRepository extends JpaRepository<StationForItinerary, Long>{
	public List<StationForItinerary> findAll();
	
	public StationForItinerary findById(@Param("id") Long id);

}
