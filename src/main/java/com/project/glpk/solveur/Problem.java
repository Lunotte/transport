package com.project.glpk.solveur;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.GlpkCallback;
import org.gnu.glpk.GlpkCallbackListener;
import org.gnu.glpk.GlpkTerminal;
import org.gnu.glpk.GlpkTerminalListener;
import org.gnu.glpk.glp_iocp;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_tran;
import org.gnu.glpk.glp_tree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.glpk.model.Detail;
import com.project.glpk.model.Itinerary;
import com.project.glpk.model.Objective;
import com.project.glpk.model.Paire;
import com.project.glpk.model.Station;
import com.project.glpk.service.DetailService;

@Component
public class Problem implements GlpkCallbackListener, GlpkTerminalListener {
	
	private boolean hookUsed = false;
    private static List<Paire> paires;
    private static Itinerary itinerary;
    private static List<Detail> allDetails;
    private static Station start;
    private static Station end;
    
    /**
     * 
     * @param i : Nombre de stations
     * @param details : Toutes les stations de tous les réseaux
     * @param stationStart : Station de départ
     * @param stationEnd : Station d'arrivée
     * @return
     */
    public static Objective main(int i, List<Detail> details, Station stationStart, Station stationEnd) {

    	allDetails = details;
    	start = stationStart;
    	end = stationEnd;
    	
    	Generator.generationProblemeDecoupe(i, details, stationStart, stationEnd);
        String[] nomeArquivo = new String[1];
        nomeArquivo[0] = "program.mod";

        GLPK.glp_java_set_numeric_locale("C");
        new Problem().solve(nomeArquivo);
        
        
        Objective obj = new Objective(itinerary, paires);
        return obj;
    }

    public synchronized void solve(String[] arg) {
        glp_prob lp = null;
        glp_tran tran;
        glp_iocp iocp;

        String fname;
       // String fname_data;
        int skip = 0;
        int ret;

        // listen to callbacks
        GlpkCallback.addListener(this);
        // listen to terminal output
        GlpkTerminal.addListener(this);

        fname = arg[0];
        //fname_data = arg[1];

        lp = GLPK.glp_create_prob();
        System.out.println("Problem created");
        tran = GLPK.glp_mpl_alloc_wksp();
        ret = GLPK.glp_mpl_read_model(tran, fname, skip);
        if (ret != 0) {
            GLPK.glp_mpl_free_wksp(tran);
            GLPK.glp_delete_prob(lp);
            throw new RuntimeException("Model file not found: " + fname);
        }
        
        // generate model
        GLPK.glp_mpl_generate(tran, null);
        // build model
        GLPK.glp_mpl_build_prob(tran, lp);
        // set solver parameters
        iocp = new glp_iocp();
        GLPK.glp_init_iocp(iocp);
        iocp.setPresolve(GLPKConstants.GLP_ON);

        // do not listen to output anymore
        GlpkTerminal.removeListener(this);
        // solve model
        ret = GLPK.glp_intopt(lp, iocp);
        // postsolve model
        if (ret == 0) {
            GLPK.glp_mpl_postsolve(tran, lp, GLPKConstants.GLP_MIP);
        }
        
       // write_lp_solution(lp);
        write_mip_solution(lp);
        // free memory
        GLPK.glp_mpl_free_wksp(tran);
        GLPK.glp_delete_prob(lp);
        // do not listen for callbacks anymore
        GlpkCallback.removeListener(this);
        
        // check that the hook function has been used for terminal output.
        if (!hookUsed) {
            System.out.println("Error: The terminal output hook was not used.");
           // System.exit(1);
        }
    }
    
    public void write_lp_solution(glp_prob lp) {
		int i;
		int n;
		String name;
		double val;

		name = GLPK.glp_get_obj_name(lp);
		val = GLPK.glp_get_obj_val(lp);
		System.out.print(name);
		System.out.print(" = ");
		System.out.println(val);
		n = GLPK.glp_get_num_cols(lp);
		for (i = 1; i <= n; i++) {
			name = GLPK.glp_get_col_name(lp, i);
			val = GLPK.glp_get_col_prim(lp, i);
			System.out.print(name);
			System.out.print(" = ");
			System.out.println(val);
		}
	}
	
	
    public void write_mip_solution(glp_prob lp)
	  {
	    int i;
	    int n;
	    String name;
	    double val;
	    
	    name = GLPK.glp_get_obj_name(lp);
	    val  = GLPK.glp_mip_obj_val(lp);
	    System.out.println("Durée du parcours ("+name+")" +" : " + val );
	    int timeWay = (int)val;
	    n = GLPK.glp_get_num_cols(lp);
	    

	   // List<Station >stations  = new ArrayList<>();
	  //  Detail detailTemp = null;
	    
	    paires = new ArrayList<>();
	    
	    for(i=1; i <= n; i++)
	    {
	      name = GLPK.glp_get_col_name(lp, i);
	      val  = GLPK.glp_mip_col_val(lp, i);
	      if ( val > 0.0)
	      {
	    	  System.out.println("Coordonnées "+name + " = " + val );
	    	  System.out.println("Station n°"+i+ " est valide : "+ val);
	    	  
	    	  String[] cd = name.split(",");
		      int left = Integer.valueOf(cd[0].replace("x[", ""));
		      int right = Integer.valueOf(cd[1].replace("]", ""));
		      paires.add(new Paire(left, right));
		      
	      }
	    }
	    
	    itinerary = new Itinerary();
	    itinerary.setTimeCourse(timeWay);
	  }
    
    @Override
    public boolean output(String str) {
        hookUsed = true;
        System.out.print(str);
        return false;
    }

    @Override
    public void callback(glp_tree tree) {
        int reason = GLPK.glp_ios_reason(tree);
        if (reason == GLPKConstants.GLP_IBINGO) {
            System.out.println("Better solution found");
        }
    }

}