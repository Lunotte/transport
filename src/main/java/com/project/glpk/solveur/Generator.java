package com.project.glpk.solveur;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.project.glpk.model.Detail;
import com.project.glpk.model.Station;

public class Generator {
	
	public static void generationProblemeDecoupe(int i, List<Detail> details, Station stationStart, Station stationEnd) {
		
		Charset ENCODING = StandardCharsets.UTF_8;
		String fichierPA = "program.mod";
		StringBuilder sb = new StringBuilder();
		
		// Ecriture dans une liste de chaines puis dans un fichier du problème
		List<String> lignesLP = new ArrayList<String>();
		lignesLP.add("param n, integer, > 0;");
		lignesLP.add("/* number of nodes */\n");
		
		lignesLP.add("set E, within {i in 1..n, j in 1..n};");
		lignesLP.add("/* set of edges */\n");
		
		lignesLP.add("param c{(i,j) in E};");
		lignesLP.add("/* c[i,j] is length of edge (i,j);\n note that edge lengths are allowed "
				+ "to be of any sign (positive, negative, or zero) */\n");
		
		lignesLP.add("param s, in {1..n};");
		lignesLP.add("/* source node */\n");
		
		lignesLP.add("param t, in {1..n};");
		lignesLP.add("/* target node */\n");
		
		lignesLP.add("var x{(i,j) in E}, >= 0;");
		lignesLP.add("/* x[i,j] = 1 means that edge (i,j) belong to shortest path;\n"
				+ " x[i,j] = 0 means that edge (i,j) does not belong to shortest path;\n"
				+ " note that variables x[i,j] are binary, however, there is no need to"
				+ " declare\n them so due to the totally unimodular constraint matrix */\n");
		
		lignesLP.add("s.t. r{i in 1..n}: sum{(j,i) in E} x[j,i] + (if i = s then 1) ="
				+ " sum{(i,j) in E} x[i,j] + (if i = t then 1);");
		lignesLP.add("/* conservation conditions for unity flow from s to t;\n every feasible"
				+ " solution is a path from s to t */\n");
		
		lignesLP.add("minimize Z: sum{(i,j) in E} c[i,j] * x[i,j];");
		lignesLP.add("/* objective function is the path length to be minimized */\n");
		
		lignesLP.add("data;\n");
		
		lignesLP.add("param n := "+i+";");
		lignesLP.add("param s := "+stationStart.getId()+";");
		lignesLP.add("param t := "+stationEnd.getId()+";");
		
		sb.append("param : E	: c := ");
		for (Detail detail : details) {
			sb.append("\n\t\t"+detail.getIdStationA().getId()+" "+detail.getIdStationB().getId()
			+" \t"+detail.getTime());
		}
		sb.append(";");
		lignesLP.add(sb.toString());
		
		lignesLP.add("end;");
		
		try {
			Path path = Paths.get(fichierPA);
			Files.write(path, lignesLP, ENCODING);
		} catch (IOException e) {
			System.out.println("Probléme d'écriture du fichier");
		}
	}
}
