package com.project.glpk.endpoint;

import org.springframework.beans.factory.annotation.Autowired;

import com.project.entity.User;
import com.project.security.auth.JwtAuthenticationToken;
import com.project.security.model.UserContext;
import com.project.user.service.DatabaseUserService;

public class MasterController {

	@Autowired 
	private DatabaseUserService databaseUserService;
	
	protected User getUser(JwtAuthenticationToken token){
		UserContext user = (UserContext) token.getPrincipal();
		return databaseUserService.getByUsername(user.getUsername());
	}
}
