package com.project.glpk.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.entity.User;
import com.project.glpk.model.Detail;
import com.project.glpk.service.DetailService;
import com.project.security.auth.JwtAuthenticationToken;

@RestController
public class DetailController extends MasterController{

	@Autowired 
	private DetailService detailService;
	
	
	/**
	 * Get all details
	 * @param token
	 * @return
	 */
	@RequestMapping(value="/api/details", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Detail> details(JwtAuthenticationToken token) {
		User user = getUser(token);
		List<Detail> details = detailService.getAllDetail(user);
		return details;
    }
	
	/**
	 * Post new Detail
	 * @param detail
	 * @return
	 */
	@RequestMapping(value="/api/detail/create", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Detail createDetails(@RequestBody Detail detail) {
		detailService.saveAndFlush(detail);
		return detail;
    }
	
	/**
	 * Post update Detail
	 * @param detail
	 * @return
	 */
	@RequestMapping(value="/api/detail/update", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public Detail updateDetails(@RequestBody Detail detail) {
		detailService.saveAndFlush(detail);
		return detail;
    }
	
	/**
	 * DELETE delete detail
	 * @param detailId
	 * @return
	 */
	@RequestMapping(value="/api/detail/delete/{detailId}", method=RequestMethod.DELETE)
    public boolean deleteDetail(@PathVariable Long detailId) {
		detailService.delete(detailId);
		return true;
    }

}
