package com.project.glpk.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.entity.User;
import com.project.glpk.model.Network;
import com.project.glpk.model.Station;
import com.project.glpk.service.DetailService;
import com.project.glpk.service.NetworkService;
import com.project.glpk.service.StationService;
import com.project.security.auth.JwtAuthenticationToken;

@RestController
public class NetworkController extends MasterController{
	
	@Autowired 
	private NetworkService networkService;
	
	@Autowired 
	private StationService stationService;
	 
	@Autowired 
	private DetailService detailService;
	
	
	/**
	 * Get All Networks
	 * @return
	 */
	@RequestMapping(value="api/networks", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Network> networks(JwtAuthenticationToken token) {
		User user = getUser(token);
		List<Network> networks = networkService.findAllByUser(user);
		return networks;
    }
	
	
	/**
	 * POST new Network
	 * @param network
	 * @return
	 */
	@RequestMapping(value="/api/network/create", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Network addNetwork(JwtAuthenticationToken token, @RequestBody Network network) {
		User user = getUser(token);
		network.setUser(user);
		networkService.saveAndFlush(network);
		return network;
    }
	
	/**
	 * PUT update Network
	 * @param network
	 * @return
	 */
	@RequestMapping(value="/api/network/update", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public Network updateNetwork(JwtAuthenticationToken token, @RequestBody Network network) {
		User user = getUser(token);
		network.setUser(user);
		networkService.saveAndFlush(network);
		return network;
    }
	
	/**
	 * DELETE delete Network
	 * @param networkId
	 * @return
	 */
	@RequestMapping(value="/api/network/delete/{networkId}", method=RequestMethod.DELETE)
    public boolean deleteNetwork(@PathVariable Long networkId) {
		Network network = networkService.findById(networkId);
		List<Station> stations = stationService.findByNetworks(network);
		for (Station station : stations) {
			detailService.deleteDetailByIdStationAOrIdStationB(station);
			stationService.delete(station.getId());
		}
		networkService.delete(networkId);
		return true;
    }
	
}
