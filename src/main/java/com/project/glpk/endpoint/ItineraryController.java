package com.project.glpk.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.entity.User;
import com.project.glpk.model.Detail;
import com.project.glpk.model.Itinerary;
import com.project.glpk.model.Objective;
import com.project.glpk.model.Paire;
import com.project.glpk.model.Station;
import com.project.glpk.model.StationForItinerary;
import com.project.glpk.service.DetailService;
import com.project.glpk.service.ItineraryService;
import com.project.glpk.service.StationForItineraryService;
import com.project.glpk.service.StationService;
import com.project.glpk.solveur.Problem;
import com.project.security.auth.JwtAuthenticationToken;

@RestController
public class ItineraryController extends MasterController{

	@Autowired 
	private ItineraryService itineraryService;
	
	@Autowired 
	private StationForItineraryService stationForItineraryService;
	
	@Autowired 
	private StationService stationService;
	 
	@Autowired 
	private DetailService detailService;
	
	
	/**
	 * Get All Itineraries
	 * @return
	 */
	@RequestMapping(value="api/itineraries", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Itinerary> Itinerary() {
		List<Itinerary> Itinerary = itineraryService.getAllItinerary();
		return Itinerary;
    }
	
	/**
	 * DELETE delete itinerary
	 * @param itineraryId
	 * @return
	 */
	@RequestMapping(value="/api/itinerary/delete/{itineraryId}", method=RequestMethod.DELETE)
    public boolean deleteItinerary(@PathVariable Long itineraryId) {
		itineraryService.delete(itineraryId);
		return true;
    }
	
	/**
	 * Get All Station for Itineraries
	 * @return
	 */
	@RequestMapping(value="api/sfi", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<StationForItinerary> sfi() {
		List<StationForItinerary> sfi = stationForItineraryService.getAllStationForItinerary();
		return sfi;
    }
	
	/**
	 * Solveur from Station A to Station B by Key
	 * @param token
	 * @param start
	 * @param end
	 * @return
	 */
	@RequestMapping(value="/api/solveur/id/{start}/{end}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public Itinerary callSolveur(JwtAuthenticationToken token, @PathVariable int start, @PathVariable int end) {
		User user = getUser(token);	
		List<Detail> details = new ArrayList<Detail>();
		
		Station stationStart = stationService.findById(Long.valueOf(start));
		Station stationEnd = stationService.findById(Long.valueOf(end));
		
		return solveur(user, details, stationStart, stationEnd);
    }
	
	/**
	 * Solveur from Station A to Station B by Name
	 * @param token
	 * @param start
	 * @param end
	 * @return
	 */
	@RequestMapping(value="/api/solveur/name/{start}/{end}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public Itinerary callSolveur(JwtAuthenticationToken token, @PathVariable String start, @PathVariable String end) {
		User user = getUser(token);	
		List<Detail> details = new ArrayList<Detail>();
		
		Station stationStart = stationService.findByName(start);
		Station stationEnd = stationService.findByName(end);
		
		return solveur(user, details, stationStart, stationEnd);
    }
	
	private Itinerary solveur(User user, List<Detail> details, Station stationStart, Station stationEnd){
		
		List<Station> stationForTest = stationService.getAllStation(user);
		for (Station station : stationForTest) {
			details.addAll(station.getDetails());
		}
		
		//Résolution
		Objective obj = Problem.main(stationForTest.get(stationForTest.size()-1).getId().intValue(), details, stationStart, stationEnd);
		
		//Retour
		Itinerary itinerary = obj.getItinerary();
		itinerary.setId(Long.valueOf(1));
		
		//Read station for our itinerary
		List<StationForItinerary> stationForItinerary = stationsFromItinerary(obj);
		
		itinerary.setStationForItinerary(stationForItinerary);
		return itinerary;
	}
	
	private List<StationForItinerary> stationsFromItinerary(Objective obj){
		List<StationForItinerary> stationForItinerary = new ArrayList<>();
		
		boolean notPassed = true;
		StationForItinerary sfi = null;
		
		for (Paire paire : obj.getPaires()) {
			Station s1 = stationService.findById(paire.getLeft());
			Station s2 = stationService.findById(paire.getRight());
			Detail detail = detailService.findByIdStationAAndIdStationB(s1, s2);
			
			if(notPassed){
				sfi = new StationForItinerary(detail.getIdStationA().getId(), detail.getIdStationA().getName(), 
					detail.getIdStationA().getRank(), new Detail(Long.valueOf(1), 0));
				stationForItinerary.add(sfi);
				notPassed = false;
			}
				
			sfi = new StationForItinerary(detail.getIdStationB().getId(), detail.getIdStationB().getName(), 
					detail.getIdStationB().getRank(), detail);
			
			stationForItinerary.add(sfi);
		}
		
		return stationForItinerary;
	}
	
}
