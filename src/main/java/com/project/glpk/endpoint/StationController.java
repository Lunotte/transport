package com.project.glpk.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.entity.User;
import com.project.glpk.model.Network;
import com.project.glpk.model.Station;
import com.project.glpk.service.DetailService;
import com.project.glpk.service.StationService;
import com.project.security.auth.JwtAuthenticationToken;


@RestController
public class StationController extends MasterController{
	
	@Autowired 
	private StationService stationService;
	 
	@Autowired 
	private DetailService detailService;
	
	
	/**
	 * Getting Started
	 * @return
	 */
	@RequestMapping(value = "/")
	public String get() {
	    return "Welcome, You can started to use the application for find the better solution";
	}
	
	
	/**
	 * Get All Stations
	 * @return
	 */
	@RequestMapping(value="api/stations", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Station> stations(JwtAuthenticationToken token) {
		User user = getUser(token);
		List<Station> stations = stationService.getAllStation(user);
		return stations;
    }
	
	@RequestMapping(value="/api/station", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Network stationsDetail(@RequestBody Station station) {
		Network network = stationService.findById(station.getId()).getNetworks().get(0);
		return network;
    }
	
	/**
	 * Post new Station
	 * @param network
	 * @return
	 */
	@RequestMapping(value="/api/station/create", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Station createStations(JwtAuthenticationToken token, @RequestBody Station station) {
		User user = getUser(token);
		station.getNetworks().get(0).setUser(user);
		stationService.saveAndFlush(station);
		return station;
    }
	
	/**
	 * Post update Station
	 * @param network
	 * @return
	 */
	@RequestMapping(value="/api/station/update", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public Station updateStations(JwtAuthenticationToken token, @RequestBody Station station) {
		User user = getUser(token);
		station.getNetworks().get(0).setUser(user);
		stationService.saveAndFlush(station);
		return station;
    }
	
	/**
	 * DELETE delete station
	 * @param stationId
	 * @return
	 */
	@RequestMapping(value="/api/station/delete/{stationId}", method=RequestMethod.DELETE)
    public boolean deleteStation(JwtAuthenticationToken token, @PathVariable Long stationId) {
		Station station = stationService.findById(stationId);
		detailService.deleteDetailByIdStationAOrIdStationB(station);
		stationService.delete(stationId);
		
		User user = getUser(token);
		updateRank(user);
		
		return true;
    }
	
	/**
	 * Update rank station for his user
	 * @param user
	 */
	private void updateRank(User user){
		List<Station> stations = stationService.getAllStation(user);
		
		int i = 1;
		for (Station station : stations) {
			if(station.getRank() != i){
				station.setRank(i);
				stationService.saveAndFlush(station);
			}
			i++;
		}
		
	}
	
}
