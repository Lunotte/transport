package com.project.glpk.tool;

public class Tool {
	
	public static int module(int numberStation){
		int mod = 0;
		int i = 2;
		boolean finish = false;
		while(i <= numberStation && !finish){
			//System.out.println(numberStation + " % " + i + " = " +numberStation % i);
			if(numberStation % i == 0){
				mod = i;
				finish = true;
			}
			i++;
		}
		return mod;
	}
}
