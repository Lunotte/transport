package com.project.glpk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.entity.User;
import com.project.glpk.model.Network;
import com.project.glpk.repository.NetworkRepository;

@Service
public class DatabaseNetworkService implements NetworkService{
	private final NetworkRepository networkRepository;
    
    @Autowired
    public DatabaseNetworkService(NetworkRepository networkRepository) {
        this.networkRepository = networkRepository;
    }
    
    public NetworkRepository getNetworkRepository() {
        return networkRepository;
    }

    @Override
    public List<Network> findAllByUser(User user) {
        return this.networkRepository.findByAuser(user);
    }
    
    @Override
	public Network findById(Long id) {
		return this.networkRepository.findById(id);
	}
    
    @Override
    public Network findByName(String name) {
        return this.networkRepository.findByName(name);
    }
    
    @Override
    public Network saveAndFlush(Network network){
    	return this.networkRepository.saveAndFlush(network);
    }

	@Override
	public void delete(Long network) {
		this.networkRepository.delete(network);
	}

}
