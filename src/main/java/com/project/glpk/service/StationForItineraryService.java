package com.project.glpk.service;

import java.util.List;

import com.project.glpk.model.StationForItinerary;

public interface StationForItineraryService {

	void delete(Long itinerary);

	StationForItinerary findById(Long id);

	List<StationForItinerary> getAllStationForItinerary();

	StationForItinerary saveAndFlush(StationForItinerary sfi);

}
