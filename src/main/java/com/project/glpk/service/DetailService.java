package com.project.glpk.service;

import java.util.List;
import java.util.Optional;

import com.project.entity.User;
import com.project.glpk.model.Detail;
import com.project.glpk.model.Station;

public interface DetailService {

	List<Detail> getAllDetail(User user);

	Detail findByIdStationAAndIdStationB(Station s1, Station s2);

	Detail saveAndFlush(Detail detail);

	void delete(Long detail);

	void deleteDetailByIdStationAOrIdStationB(Station stationId);

}
