package com.project.glpk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.entity.User;
import com.project.glpk.model.Network;
import com.project.glpk.model.Station;
import com.project.glpk.repository.StationRepository;

@Service
public class DatabaseStationService implements StationService{
	private final StationRepository stationRepository;
    
    @Autowired
    public DatabaseStationService(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }
    
    public StationRepository getStationRepository() {
        return stationRepository;
    }

    @Override
    public List<Station> getAllStation(User user) {
        return this.stationRepository.findByAuser(user);
    }

	@Override
	public Station findByName(String string) {
		return this.stationRepository.findByName(string);
	}
	
	@Override
	public Station findById(Long id) {
		return this.stationRepository.findById(id);
	}
	
	@Override
	public Station findById(int id) {
		return this.stationRepository.findById(Long.valueOf(id));
	}
	
	@Override
	public Station findTopRankByNetworksOrderByRankDesc(Network network) {
		return this.stationRepository.findTopRankByNetworksOrderByRankDesc(network);
	}
	
	@Override
	public List<Station> findByNetworks(Network network) {
		return this.stationRepository.findByNetworks(network);
	}
	
	
	
	@Override
    public Station saveAndFlush(Station station){
    	return this.stationRepository.saveAndFlush(station);
    }
	
	@Override
	public void delete(Long station) {
		this.stationRepository.delete(station);
	}
	
	/*@Override
	public void deleteUsersByIdStationAOrIdStationB(Long detailId){
		this.stationRepository.de
	}*/
}
