package com.project.glpk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.glpk.model.Itinerary;
import com.project.glpk.repository.ItineraryRepository;

@Service
public class DatabaseItineraryService implements ItineraryService{
	private final ItineraryRepository itineraryRepository;
    
    @Autowired
    public DatabaseItineraryService(ItineraryRepository itineraryRepository) {
        this.itineraryRepository = itineraryRepository;
    }
    
    public ItineraryRepository getItineraryRepository() {
        return itineraryRepository;
    }

    @Override
    public List<Itinerary> getAllItinerary() {
        return this.itineraryRepository.findAll();
    }
    
    @Override
	public Itinerary findById(Long id) {
		return this.itineraryRepository.findById(id);
	}
    
    @Override
    public Itinerary saveAndFlush(Itinerary itinerary){
    	return this.itineraryRepository.saveAndFlush(itinerary);
    }

	@Override
	public void delete(Long itinerary) {
		this.itineraryRepository.delete(itinerary);
	}

}
