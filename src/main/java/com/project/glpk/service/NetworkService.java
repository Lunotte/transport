package com.project.glpk.service;

import java.util.List;
import java.util.Optional;

import com.project.entity.User;
import com.project.glpk.model.Network;

public interface NetworkService {

	//List<Network> getAllNetwork(User user);
	Network findByName(String name);
	Network saveAndFlush(Network network);
	void delete(Long network);
	Network findById(Long id);
	List<Network> findAllByUser(User user);
}
