package com.project.glpk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.glpk.model.StationForItinerary;
import com.project.glpk.repository.StationForItineraryRepository;

@Service
public class DatabaseStationForItineraryService implements StationForItineraryService{
	private final StationForItineraryRepository stationForItineraryRepository;
    
    @Autowired
    public DatabaseStationForItineraryService(StationForItineraryRepository stationForItineraryRepository) {
        this.stationForItineraryRepository = stationForItineraryRepository;
    }
    
    public StationForItineraryRepository getItineraryRepository() {
        return stationForItineraryRepository;
    }

    @Override
    public List<StationForItinerary> getAllStationForItinerary() {
        return this.stationForItineraryRepository.findAll();
    }
    
    @Override
	public StationForItinerary findById(Long id) {
		return this.stationForItineraryRepository.findById(id);
	}
    
    @Override
    public StationForItinerary saveAndFlush(StationForItinerary sfi){
    	return this.stationForItineraryRepository.saveAndFlush(sfi);
    }

	@Override
	public void delete(Long itinerary) {
		this.stationForItineraryRepository.delete(itinerary);
	}

}
