package com.project.glpk.service;

import java.util.List;

import com.project.glpk.model.Itinerary;

public interface ItineraryService {

	List<Itinerary> getAllItinerary();

	Itinerary saveAndFlush(Itinerary itinerary);

	void delete(Long itinerary);

	Itinerary findById(Long id);
}
