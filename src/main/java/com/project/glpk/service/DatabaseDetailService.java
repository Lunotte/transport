package com.project.glpk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.entity.User;
import com.project.glpk.model.Detail;
import com.project.glpk.model.Station;
import com.project.glpk.repository.DetailRepository;

@Service
public class DatabaseDetailService implements DetailService{
	private final DetailRepository detailRepository;
    
    @Autowired
    public DatabaseDetailService(DetailRepository detailRepository) {
        this.detailRepository = detailRepository;
    }
    
    public DetailRepository getDetailRepository() {
        return detailRepository;
    }

    @Override
    public List<Detail> getAllDetail(User user) {
        return this.detailRepository.findByUser(user);
    }
    
    @Override
    public Detail findByIdStationAAndIdStationB(Station IdStationA, Station IdStationB) {
        return this.detailRepository.findByIdStationAAndIdStationB(IdStationA, IdStationB);
    }

    @Override
    public Detail saveAndFlush(Detail detail){
    	return this.detailRepository.saveAndFlush(detail);
    }
    
    @Override
	public void delete(Long detail) {
		this.detailRepository.delete(detail);
	}
    
    @Override
    public void deleteDetailByIdStationAOrIdStationB(Station stationId){
    	this.detailRepository.deleteDetailByIdStationAOrIdStationB(stationId);
    }
}
