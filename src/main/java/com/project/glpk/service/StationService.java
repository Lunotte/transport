package com.project.glpk.service;

import java.util.List;

import com.project.entity.User;
import com.project.glpk.model.Network;
import com.project.glpk.model.Station;

public interface StationService {

	List<Station> getAllStation(User user);

	Station findByName(String string);
	
	Station findTopRankByNetworksOrderByRankDesc(Network network);

	//Station findTopRankOrderByIdDesc();

	List<Station> findByNetworks(Network network);

	Station findById(int id);

	Station saveAndFlush(Station station);

	void delete(Long stationId);

	Station findById(Long id);

}
