package com.project.security.model.token;

public interface JwtToken {
    String getToken();
}
