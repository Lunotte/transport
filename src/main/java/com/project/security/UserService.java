package com.project.security;

import java.util.List;
import java.util.Optional;

import com.project.entity.User;

/**
 * 
 * @author vladimir.stankovic
 *
 * Aug 17, 2016
 */
public interface UserService {
    public User getByUsername(String username);

	List<User> getAllUser();

	Optional<User> getByUsername1(String username);
}
