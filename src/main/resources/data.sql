insert into APP_USER(ID, PASSWORD, USERNAME) values(1, '$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G', 'test1234');
--insert into USER_ROLE(APP_USER_ID, ROLE) values(1, 'ADMIN');
insert into USER_ROLE(APP_USER_ID, ROLE) values(1, 'MEMBER');

insert into NETWORK(NETWORK_ID, NAME, AUSER) values(1, 'France', 1);
insert into NETWORK(NETWORK_ID, NAME, AUSER) values(2, 'Espagne', 1);

insert into STATION(STATION_ID, NAME, RANK) values(1, 'Angers', 1);
insert into STATION(STATION_ID, NAME, RANK) values(2, 'Bordeaux', 2);
insert into STATION(STATION_ID, NAME, RANK) values(3, 'Caen', 3);
insert into STATION(STATION_ID, NAME, RANK) values(4, 'Dijon', 4);
insert into STATION(STATION_ID, NAME, RANK) values(5, 'Evreux', 5);
insert into STATION(STATION_ID, NAME, RANK) values(6, 'Fort-de-France', 6);
insert into STATION(STATION_ID, NAME, RANK) values(7, 'Grenoble', 7);

insert into STATION(STATION_ID, NAME, RANK) values(8, 'Barcelone', 8);
insert into STATION(STATION_ID, NAME, RANK) values(9, 'Madrid', 9);


insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(1, 1);
insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(2, 1);
insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(3, 1);
insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(4, 1);
insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(5, 1);
insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(6, 1);
insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(7, 1);

insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(8, 2);
insert into STATION_NETWORK(STATION_ID, NETWORK_ID) values(9, 2);


/*insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(1, 1, 2, 10);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(2, 1, 3, 20);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(3, 2, 3, 15);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(4, 5, 1, 30);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(5, 3, 4, 5);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(6, 4, 2, 30);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(7, 4, 1, 20);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(8, 6, 1, 30);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(9, 7, 4, 20);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(10, 5, 3, 10);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(11, 2, 7, 5);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(12, 5, 5, 40);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(13, 7, 7, 10);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(14, 6, 3, 5);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(15, 6, 6, 15);*/

insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(1, 1, 2, 180);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(2, 1, 3, 220);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(3, 2, 3, 96);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(4, 2, 4, 2445);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(5, 2, 5, 78);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(6, 2, 7, 1395);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(7, 3, 6, 221);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(8, 4, 5, 1508);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(9, 4, 6, 570);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(10, 4, 7, 750);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(11, 5, 7, 725);
insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(12, 6, 7, 615);

insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(13, 8, 9, 5);

insert into DETAIL(DETAIL_ID, STATION_A_ID, STATION_B_ID, TIME) values(14, 4, 9, 4000);




