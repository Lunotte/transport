param n, integer, > 0;
/* number of nodes */

set E, within {i in 1..n, j in 1..n};
/* set of edges */

param c{(i,j) in E};
/* c[i,j] is length of edge (i,j);
 note that edge lengths are allowed to be of any sign (positive, negative, or zero) */

param s, in {1..n};
/* source node */

param t, in {1..n};
/* target node */

var x{(i,j) in E}, >= 0;
/* x[i,j] = 1 means that edge (i,j) belong to shortest path;
 x[i,j] = 0 means that edge (i,j) does not belong to shortest path;
 note that variables x[i,j] are binary, however, there is no need to declare
 them so due to the totally unimodular constraint matrix */

s.t. r{i in 1..n}: sum{(j,i) in E} x[j,i] + (if i = s then 1) = sum{(i,j) in E} x[i,j] + (if i = t then 1);
/* conservation conditions for unity flow from s to t;
 every feasible solution is a path from s to t */

minimize Z: sum{(i,j) in E} c[i,j] * x[i,j];
/* objective function is the path length to be minimized */

data;

param n := 10;
param s := 1;
param t := 9;
param : E	: c := 
		1 2 	180
		1 3 	220
		2 3 	96
		2 4 	2445
		2 5 	78
		4 5 	1508
		3 6 	221
		4 6 	570
		2 7 	1395
		4 7 	750
		5 7 	725
		6 7 	615
		8 9 	5
		4 9 	4000;
end;
